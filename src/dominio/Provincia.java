package dominio;

import java.util.ArrayList;

public class Provincia{

	private String nombreDeProvincia;
	private ArrayList<Municipio> municipios = new ArrayList<>();	

	public String getNombreDeProvincia(){
		return nombreDeProvincia;
	}
	public void setNombreDeProvincia(String nombreDeProvincia){
		this.nombreDeProvincia = nombreDeProvincia;
	}

	public ArrayList<Municipio> getMunicipios(){
		return municipios;
	}

	public void setMunicipios(ArrayList<Municipio> municipios){
		this.municipios = municipios;
	} 

	public void addMunicipio(Municipio municipio){
		municipios.add(municipio);
	}
	public int calcularHabitantes(){
		int numeroDeHabitantesProvincia = 0;
		for(int j = 0; j < municipios.size(); j++){
			numeroDeHabitantesProvincia = numeroDeHabitantesProvincia 
				+ municipios.get(j).calcularHabitantes();
		}
		return numeroDeHabitantesProvincia;	
	}

	public String toString(){
		String texto = "La provincia " + nombreDeProvincia + " tiene " + calcularHabitantes()
			+ " habitantes en estos municipios:" + "\n";
		for (int k = 0; k < municipios.size(); k++){
			texto += municipios.get(k);
		}
		return texto;
	}
}

