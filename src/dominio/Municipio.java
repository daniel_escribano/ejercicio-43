package dominio;
import java.util.ArrayList;

public class Municipio{
	private String nombreDeMunicipio;
	private ArrayList<Localidad> localidades = new ArrayList<>();

	public String getNombreDeMunicipio(){
		return nombreDeMunicipio;
	}

	public void setNombreDeMunicipio(String nombreDeMunicipio){
		this.nombreDeMunicipio = nombreDeMunicipio;
	}

	public ArrayList<Localidad> getLocadades(){
		return localidades;
	}

	public void setLocalidades(ArrayList<Localidad> localidades){
		this.localidades = localidades;
	}

	public void addLocalidad(Localidad localidad){
		localidades.add(localidad);
	}

	public int calcularHabitantes(){
		int habitantesMunicipio = 0;
		for (int i = 0; i < localidades.size(); i++){
			habitantesMunicipio = habitantesMunicipio
				+ localidades.get(i).getNumeroDeHabitantes();
		}
		return habitantesMunicipio;
	}

	public String toString(){
		String texto = "\n El municipio " + nombreDeMunicipio + " tiene "
			+ calcularHabitantes() + " habitantes, de los cuales: ";
		for(int i = 0; i< localidades.size(); i++){
			texto += localidades.get(i);
		}
		return texto;
	}

}

