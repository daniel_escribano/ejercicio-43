package dominio;

public class Localidad{

        private String nombreDeLocalidad;
	private int numeroDeHabitantes;

        public String getNombreDeLocalidad(){
                return nombreDeLocalidad;
        }
        public void setNombreDeLocalidad(String nombreDeLocalidad){
                this.nombreDeLocalidad = nombreDeLocalidad;
        }

	public int getNumeroDeHabitantes(){
		return numeroDeHabitantes;
	}
	public void setNumeroDeHabitantes(int numeroDeHabitantes){
		this.numeroDeHabitantes = numeroDeHabitantes;
	}

        public String toString(){
                String texto = "La localidad " + nombreDeLocalidad + " tiene " + numeroDeHabitantes
			+ " habitantes. ";
		return texto;
	}
}
