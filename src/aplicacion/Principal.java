package aplicacion;
import dominio.*;
import java.util.ArrayList;

public class Principal{
	public static void main(String args[]){
		Localidad pozuelo = new Localidad();
		pozuelo.setNombreDeLocalidad("Pozuelo de Alarcón");
		pozuelo.setNumeroDeHabitantes(300000);

		Localidad aravaca = new Localidad();
		aravaca.setNombreDeLocalidad("Aravaca");
		aravaca.setNumeroDeHabitantes(150000);

		Localidad lasRozas = new Localidad();
		lasRozas.setNombreDeLocalidad("Las Rozas");
		lasRozas.setNumeroDeHabitantes(75000);

		Localidad majadahonda = new Localidad();
		majadahonda.setNombreDeLocalidad("Majadahonda");
		majadahonda.setNumeroDeHabitantes(25000);

		Municipio afueras = new Municipio();
		afueras.setNombreDeMunicipio("Afueras");
		afueras.addLocalidad(lasRozas);
		afueras.addLocalidad(majadahonda);

		Municipio madridExterior = new Municipio();
		madridExterior.setNombreDeMunicipio("Madrid Exterior");
		madridExterior.addLocalidad(pozuelo);
		madridExterior.addLocalidad(aravaca);

		Provincia comunidadDeMadrid = new Provincia();
		comunidadDeMadrid.setNombreDeProvincia("Comunidad de Madrid");
		comunidadDeMadrid.addMunicipio(afueras);
		comunidadDeMadrid.addMunicipio(madridExterior);
		System.out.println(comunidadDeMadrid);
	}
}	

